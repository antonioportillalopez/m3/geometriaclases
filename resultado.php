<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Area;
use clases\Figura;



$figura= new Figura($_GET);

echo '<svg '.$figura->getAnchoaltoarea().'>';
echo '<'.$figura->getTipo().' '.$figura->getDatos_dibujo().' ';
echo 'style="fill:'.$figura->getColorrelleno().';stroke:'.$figura->getColorlinea().';stroke-width:'.$figura->getGrosor().';fill-rule:'.$figura->getSegundorelleno().';" />';
echo '</svg>';

