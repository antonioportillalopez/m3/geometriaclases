<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Punto;

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title></title>
    </head>
    <body>
        
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
          <h1 class="display-4">Figuras Geometricas</h1>
        </div>
        <form action="resultado.php"
              target="popup"
              onsubmit="window.open('', 'popup', 'width = 800, height = 500')">

            <div class="container">
                  <div class="card mb-4 shadow-sm">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Definición de elementos del dibujo</h4>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>
                          <label for="anchoalturaarea">Introducir el área en ancho,alto </label>
                          <input type="text" id="anchoaltoarea" name="anchoaltoarea" value='height="210" width="500"' /> pixels
                      </li>
                      <li>
                          <label for="grosor">Grosor de línea: </label>
                          <input type="number" id="grosor" name="grosor" min="1" max="5" value="2"/>
                      </li>
                      <li>Color de línea:
                          <select name="colorlinea">
                              <option value="black">Negro</option>
                              <option value="purple" selected>Púrpura</option>
                          </select>
                      </li>
                      
                      <li>Color de relleno de figuras:
                          <select name="colorrelleno">
                              <option value="none">Sin fondo</option>
                              <option value="lime">Lima</option>
                              <option value="red">Rojo</option>
                              <option value="yellow" selected>Amarillo</option>
                          </select>
                      </li>
                      <li>Tipo de segundo relleno de las figuras:
                          <select name="segundorelleno">
                              <option value="evenodd" selected>Transparente</option>
                              <option value="nonzero">Igual al fondo</option>
                          </select>
                      </li>
                    </ul>
                  </div>
                      
                      <div class="card mb-4 shadow-sm">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Elección de dibujo</h4>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>Desplegable para elegir que se desea dibujar
                          <select name="tipofigura">
                              <option value="circle" selected>Círculo, introducir con formato: cx="50" cy="50" r="40"</option>
                              <option value="rect"> Rectángulo, introducir con formato: width="300" height="100"</option>
                              <option value="line">Línea, introducir con formato: x1="0" y1="0" x2="200" y2="200"</option>
                              <option value="polygon">Triángulo, introducir con formato: points="200,10 250,190 160,210"</option>
                              <option value="polygon">Triángulo 3D, introducir con formato: points="220,10 300,210 170,250 123,234"</option>
                              <option value="polygon">Estrella introducir con formato: points="100,10 40,198 190,78 10,78 160,198"</option>
                          </select>
                      </li>
                    </ul>
                  </div>
                      
              <div class="card-deck mb-3 text-center">
                <div class="card mb-4 shadow-sm">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Datos para la figura</h4>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                      <li> 
                          <label for="datosfigura">Introducir los datos de la figura tal como marca el formato ejemplo </label>
                          <input type="text" id="datosfigura" name="datosfigura"  />
                      </li>
                      
                    </ul>
                    
                  </div>
                </div>
                
                </div>
                     
                  <button type="submit" class="btn btn-lg btn-block btn-primary">Dibujar</button>
              </div>
            



            
            
        </form>
        
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>                
    </body>
</html>
