<?php
namespace clases;

class Figura extends Area {
    private $tipo;
    private $datos_dibujo;
    
    // No se añaden seter ya que no se añaden mediante el formulario directamente.
    
    function getTipo() {
        return $this->tipo;
    }

    function getDatos_dibujo() {
        return $this->datos_dibujo;
    }

    

   //    Esto geter son del padre ya que damos por sentado que el uso del uso del padre no es util, se heredan al tener sus propiedades protected
    function getAnchoaltoarea() {
        return $this->anchoaltoarea;
    }

    function getGrosor() {
        return $this->grosor;
    }

    function getColorlinea() {
        return $this->colorlinea;
    }

    function getColorrelleno() {
        return $this->colorrelleno;
    }

    function getSegundorelleno() {
        return $this->segundorelleno;
    }

    
// fin de los geter del padre
    
    function __construct(array $captura) {
        $this->tipo = $captura['tipofigura'];
        $this->datos_dibujo = $captura['datosfigura'];
       parent::__construct($captura);
    }

}
